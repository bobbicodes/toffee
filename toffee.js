import {env_set} from './src/env.js'
import {repl_env, REP} from './src/evaluator.js'

if (process.argv.length > 2) {
    env_set(repl_env, Symbol.for('*ARGV*'), process.argv.slice(3))
    REP(`(load-file "${process.argv[2]}")`)
    process.exit(0)
}
