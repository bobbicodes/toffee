import * as types from './types.js'
import * as hamt from './hamt.js'
import { BlankException, read_str } from './reader.js'
import { pr_str } from './printer.js'
import { new_env, env_set, env_get } from './env.js'
import { core_ns } from './core.js'
import { core_lib } from './lib.js'

// read
const READ = str => read_str(str)

// eval
const qq_loop = (acc, elt) => {
    if (types._list_Q(elt) && elt.length == 2
        && elt[0] === Symbol.for('splice-unquote')) {
        return [Symbol.for('concat'), elt[1], acc]
    } else {
        return [Symbol.for('cons'), quasiquote (elt), acc]
    }
}
const quasiquote = ast => {
    if (types._list_Q(ast)) {
        if (ast.length == 2 && ast[0] === Symbol.for('unquote')) {
            return ast[1]
        } else {
            return ast.reduceRight(qq_loop, [])
        }
    } else if (ast instanceof types.Vector) {
        return [Symbol.for('vec'), ast.reduceRight(qq_loop, [])]
    } else if (typeof ast === 'symbol' || ast instanceof Map) {
        return [Symbol.for('quote'), ast]
    } else {
        return ast
    }
}

function hasSymbol(str, ast) {
  return ast.flat(Infinity).includes(Symbol.for(str))
}

const dbgevalsym = Symbol.for("DEBUG-EVAL")

export const EVAL = (ast, env) => {
  //console.log(PRINT(ast))
  while (true) {
    if (dbgevalsym in env) {
        const dbgeval = env_get(env, dbgevalsym)
        if (dbgeval !== null && dbgeval !== false) {
            console.log('EVAL:', pr_str(ast, true))
        }
    }

    if (typeof ast === 'symbol') {
        return env_get(env, ast)
    } else if (types._set_Q(ast)) {
        var new_set = new Set()
        for (const item of ast) {
            new_set.add(EVAL(item, env))
        }
        return new_set
    } else if (ast instanceof types.Vector) {
        return ast.map(x => EVAL(x, env))
    } else if (ast instanceof Map) {
        let new_hm = new Map()
        ast.forEach((v, k) => new_hm.set(k, EVAL(v, env)))
        return new_hm
    } else if (types._hash_map_Q(ast)) {
        var new_hm = hamt.empty
        for (var [key, value] of ast) {
            new_hm = new_hm.set(EVAL(key, env), EVAL(ast.get(key), env))
        }
        return new_hm;
    } else if (!types._list_Q(ast)) {
        return ast
    }

    if (ast.length === 0) { return ast }

    const [a0, a1, a2, a3] = ast
    // Keyword functions:
    // If the first element is a keyword,
    // it looks up its value in its argument
    if (types._keyword_Q(a0)) {
        return EVAL([Symbol.for("get"), a1, a0], env)
    }
    // hash-maps as functions of keys
    if (types._hash_map_Q(a0)) {
        return EVAL([Symbol.for("get"), a0, a1], env)
    }
    // vectors as functions of indices
    if (a0 instanceof types.Vector) {
        return EVAL([Symbol.for("get"), a0, a1], env)
    }
    // sets check membership
    if (types._set_Q(a0)) {
        return EVAL([Symbol.for("contains?"), a0, a1], env)
    }
    switch (typeof a0 === 'symbol' ? Symbol.keyFor(a0) : Symbol(':default')) {
        case "dispatch":
            // Regex
            if (types._string_Q(a1)) {
                return new RegExp(a1, 'g')
            }
            // Anonymous function shorthand
            if (types._list_Q(a1)) {
              const params = a1.flat(Infinity).filter((x) => {
                  return typeof x === 'symbol' &&
                    Symbol.keyFor(x).startsWith('%')
                })
              const sortedParams = params.sort((a, b) => {
                  return parseInt(Symbol.keyFor(a).slice(1)) -
                    parseInt(Symbol.keyFor(b).slice(1))
                })
                return types._function(EVAL, a1, env, sortedParams);
            }
        case 'def':
            return env_set(env, a1, EVAL(a2, env))
        case 'let*':
            let let_env = new_env(env)
            for (let i=0; i < a1.length; i+=2) {
                env_set(let_env, a1[i], EVAL(a1[i+1], let_env))
            }
            env = let_env
            ast = a2
            break // continue TCO loop
        case 'quote':
            return a1
        case 'quasiquote':
            ast = quasiquote(a1)
            break // continue TCO loop
        case 'defmacro':
            if (types._string_Q(a2)) {
                var body = [Symbol.for("do")].concat(ast.slice(4))
                var func = types._function(EVAL, body, env, a3)
                func.ismacro = true;
                var meta_map = new Map()
                func.meta = meta_map
                meta_map.set("ʞdoc", a2)
                meta_map.set("ʞname", a1)
                meta_map.set("ʞarglists", a3)
                return env_set(env, a1, func);
            } else {
                var body = [Symbol.for("do")].concat(ast.slice(3))
                var func = types._function(EVAL, body, env, a2)
                func.ismacro = true;
                var meta_map = new Map()
                func.meta = meta_map
                meta_map.set("ʞname", a1)
                meta_map.set("ʞarglists", a2)
                return env_set(env, a1, func);
            }
        case 'try*':
            try {
                return EVAL(a1, env)
            } catch (exc) {
                if (a2 && a2[0] === Symbol.for('catch*')) {
                    if (exc instanceof Error) { exc = exc.message }
                    return EVAL(a2[2], new_env(env, [a2[1]], [exc]))
                } else {
                    throw exc
                }
            }
        case 'do':
            ast.slice(1, -1).map(x => EVAL(x, env))
            ast = ast[ast.length-1]
            break // continue TCO loop
        case 'if':
            let cond = EVAL(a1, env)
            if (cond === null || cond === false) {
                ast = (typeof a3 !== 'undefined') ? a3 : null
            } else {
                ast = a2
            }
            break // continue TCO loop
        case "loop*":
            var loop_body = [Symbol.for('do')].concat(ast.slice(2))
            var loop_env = new_env(env);
            var loopLocals = new types.Vector()
            for (var i = 0; i < a1.length; i += 2) {
                loopLocals.push(a1[i], EVAL(a1[i + 1], loop_env))
            }
            for (let i = 0; i < loopLocals.length; i += 2) {
                env_set(loop_env, a1[i], loopLocals[i + 1])
            }
            ast = loop_body
            env = loop_env
            break
        case "recur":
            var recurForms = ast.slice(1).flatMap(i => [i, i])
            for (let i = 1; i < recurForms.length; i += 2) {
                let f = recurForms[i]
                loopLocals[i] = f
            }
            ast = [Symbol.for('loop*')].concat([loopLocals, loop_body])
            break
        case 'fn*':
            // need to check each function arity for presence of
            // recur without loop bindings. If found, add implicit
            // bindings to *each* function body that needs them
            if (hasSymbol('recur', ast)) {
              let bodies = []
              for (const body of ast.slice(1)) {
                if (hasSymbol('recur', body) &&
                    !hasSymbol('loop', body) &&
                    !hasSymbol('loop*', body)) {
                  var loopBindings = types.Vector.from(body[0]
                    // exclude '&' params
                    .flatMap((i) => i != Symbol.for('&') ? [i, i] : []))
                  bodies.push(body.slice(0, 1)
                    .concat([[Symbol.for('loop*'), loopBindings,
                      [types._symbol('do')].concat(body.slice(1))]]))
                } else {
                  bodies.push(body)
                }
              }
              return types.multifn(EVAL, bodies, env);
            }
            if (types._list_Q(a1)) {
                return types.multifn(EVAL, ast.slice(1), env);
            } else {
                return types._function(EVAL, a2, env, a1);
            }
        default:
            const f = EVAL(a0, env)
            if (f.ismacro) {
                ast = f(...ast.slice(1))
                break // continue TCO loop
            }
            //console.log(ast)
            const args = ast.slice(1).map(x => EVAL(x, env))
            if (types._malfunc_Q(f)) {
                //console.log('calling fn:', PRINT(ast))
                if (f.multifn) {
                  ast = f.ast(args)
                  env = f.gen_env(args);
                  break // continue TCO loop
                } else {
                  ast = f.ast;
                  env = f.gen_env(args);
                  break // continue TCO loop
                }
            } else {
                if (types._set_Q(f) || types._keyword_Q(f) || f instanceof types.Vector || types._hash_map_Q(f)) {
                    return EVAL([f].concat(args), env)
                }
                return f(...args)
            }
        }
    }
}

// print
const PRINT = exp => pr_str(exp, true)

// repl
export let repl_env = new_env()
export const REP = str => PRINT(EVAL(READ(str), repl_env))

// core.EXT: defined using ES6
for (let [k, v] of core_ns) { env_set(repl_env, Symbol.for(k), v) }
env_set(repl_env, Symbol.for('eval'), a => EVAL(a, repl_env))
env_set(repl_env, Symbol.for('*ARGV*'), [])

// core.mal: defined using language itself
REP('(def *host-language* "ecmascript6")')
REP('(def not (fn* [a] (if a false true)))')
REP('(def load-file (fn* [f] (eval (read-string (str "(do " (slurp f) "\nnil)")))))')
REP('(defmacro cond [& xs] (if (> (count xs) 0) (list \'if (first xs) (if (> (count xs) 1) (nth xs 1) (throw "odd number of forms to cond")) (cons \'cond (rest (rest xs))))))')
REP('(do ' + core_lib + ')')
