import { _equal_Q, _clone, _keyword, _keyword_Q } from './types.js'
import * as hamt from './hamt.js'
import * as types from './types.js'
import { pr_str } from './printer.js'
import * as readline from 'node:readline';
import { read_str } from './reader.js'
import { repl_env, EVAL } from './evaluator.js';
import { readFileSync } from 'fs'

function _error(e) { throw new Error(e) }

// String functions
function slurp(f) {
    if (typeof process !== 'undefined') {
        return readFileSync(f, 'utf-8')
    } else {
        var req = new XMLHttpRequest()
        req.open('GET', f, false)
        req.send()
        if (req.status !== 200) {
            _error(`Failed to slurp file: ${f}`)
        }
        return req.responseText
    }
}

// Sequence functions
function seq(obj) {
    if (types._list_Q(obj)) {
        return obj.length > 0 ? obj : null
    } else if (obj instanceof types.Vector) {
        return obj.length > 0 ? Array.from(obj.slice(0)) : null
    } else if (typeof obj === "string" && !types._keyword_Q(obj)) {
        return obj.length > 0 ? obj.split('') : null
    } else if (obj === null) {
        return null
    } else if (obj.__hamt_isMap) {
        return Array.from(obj.entries())
    } else {
        _error('seq: called on non-sequence')
    }
}

function last(lst) { return (lst === null || lst.length === 0) ? null : seq(lst)[seq(lst).length - 1]; }

function concat(lst) {
    lst = lst || [];
    return lst.concat.apply(lst, Array.prototype.slice.call(arguments, 1));
}

function conj(lst) {
    if (types._list_Q(lst)) {
        return Array.prototype.slice.call(arguments, 1).reverse().concat(lst);
    } else if (lst instanceof types.Vector) {
        var v = lst.concat(Array.prototype.slice.call(arguments, 1));
        v.__isvector__ = true;
        return v;
    } else if (types._set_Q(lst)) {
        return lst.add(arguments[1])
    } else if (types._hash_map_Q(lst)) {
        if (arguments.length === 2 && arguments[1].size === 0) {
            return lst
        }
        var hm = hamt.empty
        const args = Array.prototype.slice.call(arguments, 1)
        let seqs = []
        for (const arg of args) {
            if (types._hash_map_Q(arg)) {
                seqs = seqs.concat(seq(arg))
            } else {
                seqs.push(arg)
            }
        }
        for (var i = 0; i < seqs.length; i++) {
            hm = hm.set(seqs[i][0], seqs[i][1])
        }
        return hm
    }
}

function _map(f, colls) {
  var map_fn = f
  var n = colls.length
  var r = [...Array(n).keys()]
  var loopKeys = r.map(x => Symbol.for('s' + x))
  var loopVals = colls.map(x => seq(x))
  var bindings = r.flatMap((n) => {
    var v = types.Vector.from(loopVals[n])
    var b = [loopKeys[n], v]
    return types.Vector.from(b)
  })
  var res = new types.Vector()
  bindings.push(Symbol.for('res'), res)
  var pred = [Symbol.for('or')]
    .concat(loopKeys.map(x => [Symbol.for('empty?'), x]))
  var recur1 = [Symbol.for('recur')]
    .concat(loopKeys.map(x => [Symbol.for('rest'), x]))
  var recur2 = [Symbol.for('conj'), Symbol.for('res')]
  recur2.push([map_fn]
    .concat(loopKeys.map(x => [Symbol.for('first'),
      x
    ])))
  var recur = recur1.concat([recur2])
  var loop = [Symbol.for('loop')]
  loop.push(bindings)
  loop.push([Symbol.for('if'), pred,
    [Symbol.for('apply'), Symbol.for('list'),
      Symbol.for('res')],
    recur
  ])
  return EVAL(loop, repl_env)
}

function _filter(f, lst) {
    if (!lst || lst.length === 0) {
        return []
    }
    if (types._set_Q(f)) {
        return seq(lst).filter(function (el) { return f.has(el); });
    }
    return seq(lst).filter(function (el) { return f(el); })
}

const with_meta = (a,b) => {
  //console.log("Attaching metadata", b, "to", a)
  let c = _clone(a); c.meta = b; return c
}

export function take(n, coll) {
    if (empty_Q(coll)) {
        return []
    }
    return seq(coll).slice(0, n)
}

function drop(n, coll) {
    if (coll === null) {
        return []
    }
    if (empty_Q(coll)) {
        return []
    }
    return seq(coll).slice(n)
}

function _subvec(v, start, end) {
    return v.slice(start, end)
}

function range(start, end, step) {
    if (step < 0) {
        var ans = [];
        for (let i = start; i > end; i += step) {
            ans.push(i);
        }
        return ans
    }
    if (!end) {
        if (start === 0) {
            return []
        }
        return range(0, start)
    }
    var ans = [];
    if (step) {
        for (let i = start; i < end; i += step) {
            ans.push(i);
        }
        return ans
    }
    for (let i = start; i < end; i++) {
        ans.push(i);
    }
    return ans;
}

export function get(coll, key, notfound) {
    if (coll instanceof types.Vector) {
        return coll[key]
    }
    if (types._string_Q(coll)) {
        return coll[key]
    }
    if (types._hash_map_Q(coll)) {
        for (const [k, value] of coll) {
            if (types._equal_Q(k, key)) {
                return value
            }
        }
        return notfound || null
    }
    if (coll != null) {
        return coll.get(key) || notfound
    } else {
        return null;
    }
}

function dissoc(hm) {
    let new_hm = hm
    for (var i = 1; i < arguments.length; i++) {
        var ktoken = arguments[i];
        new_hm = new_hm.remove(ktoken)
    }
    return new_hm
}

function empty_Q(lst) {
    if (types._set_Q(lst) || types._hash_map_Q(lst)) {
        if (lst.size === 0) {
            return true
        } else {
            return false
        }
    }
    if (!lst) {
        return true
    }
    return lst.length === 0;
}

function contains_Q(coll, key) {
    if (coll === null) {
        return false
    }
    if (types._hash_map_Q(coll)) {
        for (const [k, value] of Array.from(coll.entries)) {
            if (types._equal_Q(k, key)) {
                return true
            }
        }
    }
    if (types._set_Q(coll)) {
        for (const item of coll) {
            if (types._equal_Q(item, key)) {
                return true
            }
        }
        return false
    }
    if (key in coll) { return true; } else { return false; }
}

function _println() {
    console.log.apply(console, arguments)
}

function consolePrint() {
    _println.apply({}, Array.prototype.map.call(arguments, function (exp) {
        return pr_str(exp, false);
    }));
}

function first(lst) {
  return (lst === null || lst.length === 0) ? null : seq(lst)[0];
}

function _substring(s, start, end) {
    if (!end) {
        return s.substring(start, s.length)
    }
    return s.substring(start, end)
}

function sum() {
    if (Array.from(arguments).length === 0) {
        return 0
    }
    return Array.from(arguments).reduce((acc, a) => acc + a, 0);
}

function rest(lst) {
    return (lst == null || lst.length === 0) ? [] : seq(lst).slice(1);
}

function toSet(coll) {
    var new_set = new Set()
    for (const item of seq(coll)) {
        if (!contains_Q(new_set, item)) {
            new_set.add(item)
        }
    }
    console.log(new_set)
    return new_set
}

function repeat(n, x) {
  return Array(n).fill(x);
}

function _postMessage(m) {
  // convert map to object
  const obj = Object.fromEntries(m)
  // convert keywords to strings
  let new_obj = {}
  for (const prop in obj) {
    new_obj[(prop[0] === 'ʞ') ? prop.slice(1) : prop] = obj[prop]
  }
  postMessage(new_obj)
}

// core_ns is namespace of type functions
export const core_ns = new Map([
    ['=', _equal_Q],
    ['throw', a => { throw a }],
    ['console-print', consolePrint],
    ['post-message', _postMessage],

    ['nil?', a => a === null],
    ['true?', a => a === true],
    ['false?', a => a === false],
    ['number?', a => typeof a === 'number'],
    ['string?', a => typeof a === "string" && !_keyword_Q(a)],
    ['symbol', a => Symbol.for(a)],
    ['symbol?', a => typeof a === 'symbol'],
    ['keyword', _keyword],
    ['keyword?', _keyword_Q],
    ['fn?', a => typeof a === 'function' && !a.ismacro ],
    ['macro?', a => typeof a === 'function' && !!a.ismacro ],
    ['vector?', a => a instanceof types.Vector ],

    ['pr-str', (...a) => a.map(e => pr_str(e,1)).join(' ')],
    ['str', (...a) => a.map(e => pr_str(e,0)).join('')],
    ['prn', (...a) => console.log(...a.map(e => pr_str(e,1))) || null],
    ['println', (...a) => console.log(...a.map(e => pr_str(e,0))) || null],
    ['read-string', read_str],
    ['readline', readline],
    ['slurp', slurp],

    ['<' , (a,b) => a<b],
    ['<=', (a,b) => a<=b],
    ['>' , (a,b) => a>b],
    ['>=', (a,b) => a>=b],
    ['+' , sum],
    ['-' , (a,b) => a-b],
    ['*' , (a,b) => a*b],
    ['/' , (a,b) => a/b],
    ["time-ms", () => new Date().getTime()],

    ['list', (...a) => a],
    ['list?', types._list_Q],
    ['set', toSet],
    ['set?', types._set_Q],
    ['vector', (...a) => Vector.from(a)],
    ['vector?', a => a instanceof types.Vector],
    ['hash-map', (...a) => types._assoc_BANG(hamt.empty, ...a)],
    ['map?', a => types._hash_map_Q(a)],
    ['assoc', (m,...a) => types._assoc_BANG(m, ...a)],
    ['dissoc', dissoc],
    ['get', get],
    ['contains?', contains_Q],
    ['take', take],
    ['drop', drop],
    ['subvec', _subvec],
    ['range', range],
    ['keys', a => Array.from(a.keys())],
    ['vals', a => Array.from(a.values())],
    ['subs', _substring],

    ['sequential?', a => Array.isArray(a)],
    ['cons', (a,b) => [a].concat(b)],
    ['repeat', repeat],
    ['concat', concat],
    ['cat', concat],
    ['vec', (a) => types.Vector.from(a)],
    ['nth', (a,b) => b < a.length ? a[b] : _error('nth: index out of range')],
    ['first', first],
    ['rest', rest],
    ['last', last],
    ['empty?', empty_Q],
    ['count', a => a === null ? 0 : a.length],
    ['apply*', (f,...a) => f(...a.slice(0, -1).concat(a[a.length-1]))],
    ['_map', _map],
    ['filter', _filter],
    ['includes?', (s,substr) => s.includes(substr)],

    ['conj', conj],
    ['seq', seq],

    ['meta', a => 'meta' in a ? a['meta'] : null],
    ['with-meta', with_meta],
    ['atom', a => new types.Atom(a)],
    ['atom?', a => a instanceof types.Atom],
    ['deref', atm => atm.val],
    ['reset!', (atm,a) => atm.val = a],
    ['swap!', (atm,f,...args) => atm.val = f(...[atm.val].concat(args))]
    ])
