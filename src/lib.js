export const core_lib = `

(def inc (fn* [a] (+ a 1)))

(defmacro or
  "Evaluates exprs one at a time, from left to right. If a form
    returns a logical true value, or returns that value and doesn't
    evaluate any of the other expressions, otherwise it returns the
    value of the last expression. (or) returns nil."
  [& xs]
  (if (empty? xs) nil
      (if (= 1 (count xs))
        (first xs)
        (let* [condvar (gensym)]
              \`(let* [~condvar ~(first xs)]
                     (if ~condvar ~condvar (or ~@(rest xs))))))))

(defmacro and
  "Evaluates exprs one at a time, from left to right. If a form
    returns logical false (nil or false), and returns that value and
    doesn't evaluate any of the other expressions, otherwise it returns
    the value of the last expr. (and) returns true."
  [& xs]
  (cond (empty? xs)      true
        (= 1 (count xs)) (first xs)
        :else
        (let* [condvar (gensym)]
              \`(let* [~condvar ~(first xs)]
                     (if ~condvar (and ~@(rest xs)) ~condvar)))))

(defmacro when
  "Evaluates test. If logical true, evaluates body in an implicit do."
  [x & xs]
  (list 'if x (cons 'do xs)))

(def spread (fn* [arglist]
                 (cond
                   (nil? arglist) nil
                   (nil? (next arglist)) (seq (first arglist))
                   :else (cons (first arglist) (spread (next arglist))))))

(def list*
  (with-meta
    (fn*
     ([args] (seq args))
     ([a args] (cons a args))
     ([a b args] (cons a (cons b args)))
     ([a b c args] (cons a (cons b (cons c args))))
     ([a b c d & more]
      (cons a (cons b (cons c (cons d (spread more)))))))
    {:doc      "Creates a new seq containing the items prepended to the rest, the
last of which will be treated as a sequence."
     :arglists '([args] [a args] [a b args] [a b c args] [a b c d & more])}))

(def apply
  (with-meta
    (fn*
     ([f args]
      (if (keyword? f)
        (get args f)
        (apply* f args)))
     ([f x args]
      (apply f (list* x args)))
     ([f x y args]
      (apply f (list* x y args)))
     ([f x y z args]
      (apply f (list* x y z args)))
     ([f a b c d & args]
      (apply f (cons a (cons b (cons c (cons d (spread args))))))))
    {:doc "Applies fn f to the argument list formed by prepending intervening arguments to args."
     :arglists '([f args] [f x args] [f x y args] [f x y z args] [f a b c d & args])}))

(def map
  (with-meta
    (fn* 
     ([f coll]
      (loop* [s (seq coll) res []]
             (if (empty? s) (apply list res)
                 (recur (rest s)
                        (conj res (if (keyword? f) (get (first s) f) (f (first s))))))))
     ([f c1 c2]
      (loop* [s1 (seq c1) s2 (seq c2) res []]
             (if (or (empty? s1) (empty? s2)) (apply list res)
                 (recur (rest s1) (rest s2)
                        (conj res (f (first s1) (first s2)))))))
     ([f c1 c2 c3]
      (loop* [s1 (seq c1) s2 (seq c2) s3 (seq c3) res []]
             (if (or (empty? s1) (empty? s2) (empty? s3)) (apply list res)
                 (recur (rest s1) (rest s2) (rest s3)
                        (conj res (f (first s1) (first s2) (first s3)))))))
     ([f c1 c2 c3 c4]
      (loop* [s1 (seq c1) s2 (seq c2) s3 (seq c3) s4 (seq c4) res []]
             (if (or (empty? s1) (empty? s2) (empty? s3) (empty? s4)) (apply list res)
                 (recur (rest s1) (rest s2) (rest s3) (rest s4)
                        (conj res (f (first s1) (first s2) (first s3) (first s4)))))))
     ([f c0 c1 c2 & colls]
      (_map f (list* c0 c1 c2 colls))))
      {:doc "Returns a sequence consisting of the result of applying f to
            the set of first items of each coll, followed by applying f to the
            set of second items in each coll, until any one of the colls is
            exhausted.  Any remaining items in other colls are ignored. Function
            f should accept number-of-colls arguments."
       :arglists '([f coll] [f c1 c2] [f c1 c2 c3] [f c1 c2 c3 c4] [f c0 c1 c2 & colls])}))

(def seq?
  (with-meta
    (fn* [x] (list? x))
    {:doc "Returns true if x can be sequenced"
     :arglists '([x])}))

(def gensym-counter
  (atom 0))

(def gensym 
  (with-meta
    (fn* 
     ([] (symbol (str "G__" (swap! gensym-counter inc))))
     ([prefix]
      (symbol (str prefix (swap! gensym-counter inc)))))
    {:doc      "Returns a new symbol with a unique name. If a prefix string is
supplied, the name is prefix# where # is some unique number. If
prefix is not supplied, the prefix is 'G__'."
     :arglists '([] [prefix])}))

;; first define let, loop, fn without destructuring

(defmacro let [bindings & body]
  \`(let* ~bindings ~@body))

(defmacro loop [& decl]
   (cons 'loop* decl))

(defmacro fn [& sigs]
  (let [name (if (symbol? (first sigs)) (first sigs) nil)
        sigs (if name (next sigs) sigs)
        sigs (if (vector? (first sigs))
               (list sigs)
               (if (seq? (first sigs))
                 sigs
                 (throw (if (seq sigs)
                          (str "Parameter declaration "
                               (first sigs)
                               " should be a vector")
                          (str "Parameter declaration missing")))))
        psig (fn* [sig]
                  sig)
        new-sigs (map psig sigs)]
    (if name
      (list* 'fn* name new-sigs)
      (cons 'fn* new-sigs))))

(def next
  (with-meta
   (fn [coll]
    (if (or (= 1 (count coll)) (= 0 (count coll)))
      nil
      (rest coll)))
    {:doc "Returns a seq of the items after the first. Calls seq on its
argument.  If there are no more items, returns nil."
     :arglists '([coll])}))

(def sigs
  (fn [fdecl]
    (let [asig
          (fn [fdecl]
            (let [arglist (first fdecl)
                  body (next fdecl)]
              (if (map? (first body))
                (if (next body)
                  (with-meta arglist (conj (if (meta arglist) (meta arglist) {}) (first body)))
                  arglist)
                arglist)))]
      (if (seq? (first fdecl))
        (loop [ret [] fdecls fdecl]
          (if fdecls
            (recur (conj ret (asig (first fdecls))) (next fdecls))
            (seq ret)))
        (list (asig fdecl))))))

(defmacro defn
  "Same as (def name (fn [params* ] exprs*)) or (def
  name (fn ([params* ] exprs*)+)) with any doc-string or attrs added
  to the var metadata."
  [name & fdecl]
  (let* [m (if (string? (first fdecl))
             {:doc  (first fdecl)
              :name (str name)}
             {:name (str name)})
         fdecl (if (string? (first fdecl))
                 (next fdecl)
                 fdecl)
         m (if (map? (first fdecl))
             (conj m (first fdecl))
             m)
         fdecl (if (map? (first fdecl))
                 (next fdecl)
                 fdecl)
         fdecl (if (vector? (first fdecl))
                 (list fdecl)
                 fdecl)
         m (if (map? (last fdecl))
             (conj m (last fdecl))
             m)
         fdecl (if (map? (last fdecl))
                 (butlast fdecl)
                 fdecl)
         m (conj {:arglists (list 'quote (sigs fdecl))} m)
       ; m (conj (if (meta name) (meta name) {}) m)
        ]
    \`(def ~name (with-meta (fn ~@fdecl) ~m))))

(defn not=
  "Same as (not (= obj1 obj2))"
  [a b]
  (not (= a b)))

(defn ffirst
  "Same as (first (first x))"
  [x]
  (first (first x)))

(defn second
  "Same as (first (next x))"
  [l] (nth l 1))

(defn complement
  "Takes a fn f and returns a fn that takes the same arguments as f,
has the same effects, if any, and returns the opposite truth value."
  [f]
  (fn*
    ([] (not (f)))
    ([x] (not (f x)))
    ([x y] (not (f x y)))
    ([x y & zs] (not (apply f x y zs)))))

(defmacro when-let
  "When test is true, evaluates body with binding-form bound to the value of test"
  [bindings & body]
  (let* [form (get bindings 0) tst (get bindings 1)
         temp# (gensym)]
        \`(let* [temp# ~tst]
               (when temp#
                 (let* [~form temp#]
                       ~@body)))))

(defmacro when-first
  "Roughly the same as (when (seq xs) (let [x (first xs)] body)) but xs is evaluated only once."
  [bindings & body]
  (let* [x   (first bindings)
         xs  (last bindings)
         xs# (gensym)]
        \`(when-let [xs# (seq ~xs)]
           (let* [~x (first xs#)]
                 ~@body))))

(defn mapcat
  "Returns the result of applying concat to the result of applying map
to f and colls.  Thus function f should return a collection."
  [f & colls]
  (apply concat (apply map f colls)))

(defn remove
  "Returns a lazy sequence of the items in coll for which
(pred item) returns logical false. pred must be free of side-effects."
  [pred coll]
  (filter (complement pred) coll))

(defn reduce
  "f should be a function of 2 arguments. If val is not supplied,
returns the result of applying f to the first 2 items in coll, then
applying f to that result and the 3rd item, etc. If coll contains no
items, f must accept no arguments as well, and reduce returns the
result of calling f with no arguments. If coll has only 1 item, it
is returned and f is not called.  If val is supplied, returns the
result of applying f to val and the first item in coll, then
applying f to that result and the 2nd item, etc. If coll contains no
items, returns val and f is not called."
  ([f xs] (reduce f (first xs) (rest xs)))
  ([f init xs]
   (if (empty? xs)
     init
     (reduce f (f init (first xs)) (rest xs)))))

(defn partition
  "Returns a lazy sequence of lists of n items each, at offsets step
apart. If step is not supplied, defaults to n, i.e. the partitions
do not overlap. If a pad collection is supplied, use its elements as
necessary to complete last partition upto n items. In case there are
not enough padding elements, return a partition with less than n items."
  ([n coll]
   (partition n n coll))
  ([n step coll]
     (loop* [s coll p []]
       (if (= 0 (count s))
         (filter #(= n (count %)) p)
         (recur (drop step s) (conj p (take n s))))))
  ([n step pad coll]
     (loop* [s coll p []]
           (if (= n (count (take n s)))
             (recur (drop step s) (conj p (take n s)))
             (conj p (concat (take n s) pad))))))

(defn emit-for [bindings body-expr]
  (let [giter (gensym "iter__")
        gxs (gensym "s__")
        iterys# (gensym "iterys__")
        fs#     (gensym "fs__")
        ;; TODO: create named lambdas so won't need to do this
        do-mod (defn do-mod [mod]
                 (cond
                   (= (ffirst mod) :let) \`(let ~(second (first mod)) 
                                            ~(do-mod (next mod)))
                   (= (ffirst mod) :while) \`(when ~(second (first mod)) 
                                              ~(do-mod (next mod)))
                   (= (ffirst mod) :when) \`(if ~(second (first mod))
                                             ~(do-mod (next mod))
                                             (recur (rest ~gxs)))
                   (keyword?  (ffirst mod)) (throw (str "Invalid 'for' keyword " (ffirst mod)))
                   (next bindings)
                   \`(let [~iterys# ~(emit-for (next bindings) body-expr)
                          ~fs# (seq (~iterys# ~(second (first (next bindings)))))]
                      (if ~fs#
                        (concat ~fs# (~giter (rest ~gxs)))
                        (recur (rest ~gxs))))
                   :else \`(cons ~body-expr (~giter (rest ~gxs)))))]
    (if (next bindings)
      \`(defn ~giter [~gxs]
         (loop [~gxs ~gxs]
           (when-first [~(ffirst bindings) ~gxs]
             ~(do-mod (subvec (first bindings) 2)))))
      \`(defn ~giter [~gxs]
         (loop [~gxs ~gxs]
           (when-let [~gxs (seq ~gxs)]
             (let [~(ffirst bindings) (first ~gxs)]
               ~(do-mod (subvec (first bindings) 2)))))))))

(defmacro for
  "List comprehension. Takes a vector of one or more
 binding-form/collection-expr pairs, each followed by zero or more
 modifiers, and yields a lazy sequence of evaluations of expr.
 Collections are iterated in a nested fashion, rightmost fastest,
 and nested coll-exprs can refer to bindings created in prior
 binding-forms.  Supported modifiers are: :let [binding-form expr ...],
 :while test, :when test. Example:
 (take 100 (for [x (range 100000000) y (range 1000000) :while (< y x)] [x y]))"
  [seq-exprs body-expr]
  (let [body-expr* body-expr
        iter#      (gensym)
        to-groups  (fn* [seq-exprs]
                        (reduce (fn* [groups kv]
                                     (if (keyword? (first kv))
                                       (conj (pop groups) 
                                             (conj (peek groups) [(first kv) (last kv)]))
                                       (conj groups [(first kv) (last kv)])))
                                [] (partition 2 seq-exprs)))]
    \`(let [~iter# ~(emit-for (to-groups seq-exprs) body-expr)]
       (remove nil?
               (~iter# ~(second seq-exprs))))))

(defn key
  "Returns the key of the map entry."
  [e]
  (first e))

(defn val
  "Returns the value of the map entry."
  [e]
  (last e))

(defn butlast
  "Return a seq of all but the last item in coll, in linear time"
  [s]
  (loop* [ret []
         s   s]
    (if (next s)
      (recur (conj ret (first s)) (next s))
      (seq ret))))

(defn every?
  "Returns true if (pred x) is logical true for every x in coll, else
false."
  [pred xs]
  (cond (empty? xs)       true
        (pred (first xs)) (every? pred (rest xs))
        true              false))

(defmacro if-let
    "If test is true, evaluates then with binding-form bound to the value of 
  test, if not, yields else"
  [bindings then else & oldform]
  (if-not else
    \`(if-let ~bindings ~then nil)
    (let* [form (get bindings 0) tst (get bindings 1)
           temp# (gensym)]
          \`(let [temp# ~tst]
             (if temp#
               (let [~form temp#]
                 ~then)
               ~else)))))

(defn _iter-> [acc form]
  (if (list? form)
    \`(~(first form) ~acc ~@(rest form))
    (list form acc)))

(defmacro ->
  "Threads the expr through the forms. Inserts x as the
second item in the first form, making a list of it if it is not a
list already. If there are more forms, inserts the first form as the
second item in second form, etc."
  [x & xs]
  (reduce _iter-> x xs))

(defn _iter->> [acc form]
  (if (list? form)
    \`(~(first form) ~@(rest form) ~acc) (list form acc)))

(defmacro ->>
  "Threads the expr through the forms. Inserts x as the
last item in the first form, making a list of it if it is not a
list already. If there are more forms, inserts the first form as the
last item in second form, etc."
  [x & xs]
  (reduce _iter->> x xs))

(defn some
  "Returns the first logical true value of (pred x) for any x in coll,
else nil.  One common idiom is to use a set as pred, for example
this will return :fred if :fred is in the sequence, otherwise nil:
(some #{:fred} coll)"
  [pred xs]
  (if (set? pred)
    (if (empty? xs) nil
      (or (when (contains? pred (first xs))
            (first xs))
          (some pred (rest xs))))
    (if (map? pred)
      (if (empty? xs) nil
          (or (when (contains? pred (first xs))
                (get pred (first xs)))
              (some pred (rest xs))))
      (if (empty? xs) nil
        (or (pred (first xs))
            (some pred (rest xs)))))))

(defmacro if-not
  "Evaluates test. If logical false, evaluates and returns then expr, 
otherwise else expr, if supplied, else nil."
  [test then else]
  (if else
    \`(if (not ~test) ~then ~else)
    \`(if-not ~test ~then nil)))

(defn assoc-in
  "Associates a value in a nested associative structure, where ks is a
sequence of keys and v is the new value and returns a new nested structure.
If any levels do not exist, hash-maps will be created."
  [m ks v]
  (if (next ks)
    (assoc m (first ks) (assoc-in (get m (first ks)) (rest ks) v))
    (assoc m (first ks) v)))

(defn take-nth
  "Returns a seq of every nth item in coll."
  [n coll]
  (loop* [s coll res []]
    (if (empty? s) res
        (recur (drop n s) (conj res (first s))))))

(defn namespace
  "Returns the namespace String of a symbol or keyword, or nil if not present."
  [x]
  (when (includes? (str x) "/")
    (first (str/split (str x) "/"))))

(defn name
  "Returns the name String of a string, symbol or keyword."
  [x]
  (if (keyword? x)
    (subs (str x) 1)
    (str x)))

(defn nnext
  "Same as (next (next x))"
  [s]
  (next (next s)))

(defn destructure-vec [bvec b val]
  (let [gvec (gensym "vec__")
        gseq (gensym "seq__")
        gfirst (gensym "first__")
        has-rest (some #{'&} b)]
        (loop* [ret (let [ret (conj bvec gvec val)]
                         (if has-rest
                           (conj ret gseq (list seq gvec))
                           ret))
               n 0
               bs b
               seen-rest? false]
          (if (seq bs)
            (let [firstb (first bs)]
                  (cond
                    (= firstb '&) (recur (destructure* ret (second bs) gseq)
                                         n
                                         (nnext bs)
                                         true)
                    (= firstb :as) (destructure* ret (second bs) gvec)
                    :else (if seen-rest?
                            (throw "Unsupported binding form, only :as can follow & parameter")
                            (recur (destructure* (if has-rest
                                         (conj ret
                                               gfirst \`(~first ~gseq)
                                               gseq \`(~next ~gseq))
                                         ret)
                                       firstb
                                       (if has-rest
                                         gfirst
                                         (list 'nth gvec n nil)))
                                   (inc n)
                                   (next bs)
                                   seen-rest?))))
            ret))))

(defn destructure-map [bvec b v]
  (let* [gmap (gensym "map__")
         defaults (:or b)]
        (loop* [ret (-> bvec (conj gmap) (conj v)
                       (conj gmap) (conj gmap)
                       ((fn* [ret]
                          (if (:as b)
                            (conj ret (:as b) gmap)
                            ret))))
               bes (let* [transforms
                          (reduce
                           (fn* [transforms mk]
                             (if (keyword? mk)
                               (let* [mkns (namespace mk)
                                      mkn (name mk)]
                                     (cond (= mkn "keys") (assoc transforms mk (fn* [k] (keyword (or mkns (namespace k)) (name k))))
                                           (= mkn "syms") (assoc transforms mk (fn* [k] (list \`quote (symbol (or mkns (namespace k)) (name k)))))
                                           (= mkn "strs") (assoc transforms mk str)
                                           :else transforms))
                               transforms))
                           {}
                           (keys b))]
                         (reduce
                          (fn* [bes entry] (reduce (fn* [a b] (assoc a b ((val entry) b))) (dissoc bes (key entry)) (get bes (key entry))))
                          (dissoc b :as :or)
                          transforms))]
          bes
          (if (seq bes)
            (let* [bb (key (first bes))
                   bk (val (first bes))
                   local bb
                   bv (if (contains? defaults local)
                        (list \`get gmap bk (get defaults local))
                        (list \`get gmap bk))]
                  (recur
                   (if (or (keyword? bb) (symbol? bb))
                     (-> ret (conj local bv))
                     (destructure* ret bb bv))
                   (next bes)))
            ret))))

(defn destructure* [bvec b v]
  (cond
    (symbol? b) (-> bvec (conj (if (namespace b)
                                 (symbol (name b)) b)) (conj v))
    (keyword? b) (-> bvec (conj (symbol (name b))) (conj v))
    (vector? b) (destructure-vec bvec b v)
    (map? b) (destructure-map bvec b v)
    :else (throw (str "Unsupported binding form: " b))))

(defn destructure
  "Takes a binding form and outputs bindings with all destructuring forms expanded."
  [bindings]
  (let* [bents (partition 2 bindings)
         process-entry (fn* [bvec b] (destructure* bvec (first b) (second b)))]
        (if (every? symbol? (map first bents))
          bindings
          (if-let [kwbs (seq (filter #(keyword? (first %)) bents))]
            (throw (str "Unsupported binding key: " (ffirst kwbs)))
            (reduce process-entry [] bents)))))

(defmacro let
    "Evaluates the exprs in a lexical context in which the symbols in
  the binding-forms are bound to their respective init-exprs or parts
  therein."
  [bindings & body]
  \`(let* ~(destructure bindings) ~@body))

(defmacro loop
  "Evaluates the exprs in a lexical context in which the symbols in
the binding-forms are bound to their respective init-exprs or parts
therein. Acts as a recur target."
  [bindings & body]
  (let [db (destructure bindings)]
    (if (= db bindings)
      \`(loop* ~bindings ~@body)
      (let [vs (take-nth 2 (drop 1 bindings))
            bs (take-nth 2 bindings)
            gs (map (fn [b] (if (symbol? b) b (gensym))) bs)
            bfs (reduce (fn [ret [b v g]]
                           (if (symbol? b)
                             (conj ret g v)
                             (conj ret g v b g)))
                         [] (map vector bs vs gs))]
        \`(let ~bfs
           (loop* ~(vec (interleave gs gs))
                  (let ~(vec (interleave bs gs))
                    ~@body)))))))

(defn maybe-destructured [params body]
  (if (every? symbol? params)
    (cons params body)
    (loop* [params params
           new-params (with-meta [] (meta params))
           lets []]
      (if params
        (if (symbol? (first params))
          (recur (next params) (conj new-params (first params)) lets)
          (let [gparam (gensym "p__")]
            (recur (next params) (conj new-params gparam)
                   (-> lets (conj (first params)) (conj gparam)))))
        \`(~new-params
          (let ~lets
            ~@body))))))

;redefine fn with destructuring
(defmacro fn
  "Defines a function."
  [& sigs]
  (let [name (if (symbol? (first sigs)) (first sigs) nil)
        sigs (if name (next sigs) sigs)
        sigs (if (vector? (first sigs))
               (list sigs)
               (if (seq? (first sigs))
                 sigs
                 (throw (if (seq sigs)
                          (str "Parameter declaration "
                               (first sigs)
                               " should be a vector")
                          (str "Parameter declaration missing")))))
        psig (fn* [sig]
                  (let [[params & body] sig]
                    (maybe-destructured params body)))
        new-sigs (map psig sigs)]
    (if name
      (list* 'fn* name new-sigs)
      (cons 'fn* new-sigs))))
`
