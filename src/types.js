import * as hamt from './hamt.js'
import {new_env} from './env.js'

// General functions
export function _equal_Q (a, b) {
    if (typeof a === 'set') {
      return a.size === b.size && [...a].every((x) => {
        for (const item of b) {
            if (_equal_Q(item, x)) {
                return true
            }
        }
        return false
    });
    }
    if (Array.isArray(a) && Array.isArray(b)) {
        if (a.length !== b.length) { return false }
        for (let i=0; i<a.length; i++) {
            if (! _equal_Q(a[i], b[i])) { return false }
        }
        return true
    } else if (a instanceof Map && b instanceof Map) {
        if (a.size !== b.size) { return false }
        for (let k of a.keys()) {
            if (! _equal_Q(a.get(k), b.get(k))) { return false }
        }
        return true
    } else {
        return a === b
    }
}

export function _clone(obj, new_meta) {
    let new_obj = null
    if (_list_Q(obj)) {
        new_obj = obj.slice(0)
    } else if (obj instanceof Vector) {
        new_obj = Vector.from(obj)
    } else if (obj instanceof Map) {
        new_obj = new Map(obj.entries())
    } else if (typeof obj === 'symbol') {
        new_obj = obj
    } else if (obj.ast) { // function
        new_obj = obj
    } else {
        throw Error('Unsupported type for clone')
    }
    if (typeof new_meta !== 'undefined') { new_obj.meta = new_meta }
    return new_obj
}

// Scalars
export function _nil_Q(a) { return a === null ? true : false; }
export function _true_Q(a) { return a === true ? true : false; }
export function _false_Q(a) { return a === false ? true : false; }
export function _number_Q(obj) { return typeof obj === 'number'; }
export function _string_Q(obj) {
    return typeof obj === 'string' && obj[0] !== '\u029e';
}

// Functions
export function _malfunc(f, ast, env, params, meta=null, ismacro=false) {
    return Object.assign(f, {ast, env, params, meta, ismacro})
}
export const _malfunc_Q = f => (typeof f !== 'undefined' && f !== null && f.ast) ? true : false

export function _function(Eval, ast, env, params) {
    var fn = function () {
        return Eval(ast, new_env(env, params, Array.from(arguments)));
    };
    fn.meta = null;
    fn.ast = ast;
    fn.gen_env = function (args) { return new_env(env, params, args); };
    fn.ismacro = false;
    return fn;
}

function findVariadic(bodies) {
    for (let i = 0; i < bodies.length; i++) {
        let hasRestParam = false
        for (const sym of bodies[i][0]) {
          if (Symbol.keyFor(sym) === "&") {hasRestParam = true}
        }
        if (hasRestParam) {
            return bodies[i]
        }
    }
}

function findFixedArity(arity, bodies) {
    for (let i = 0; i < bodies.length; i++) {
        let hasRestParam = false
        for (const sym of bodies[i][0]) {
          if (Symbol.keyFor(sym) === "&") {hasRestParam = true}
        }
        if (bodies[i][0].length === arity && !hasRestParam) {
            return bodies[i]
        }
    }
}

export function multifn(Eval, bodies, env) {
    var fn = function () {
        var arity = arguments.length
        var body = findFixedArity(arity, bodies) || findVariadic(bodies)
        return Eval(body[1],
            new_env(env, body[0], Array.from(arguments)));
    }
    fn.meta = null;
    fn.multifn = true
    fn.ast = function (args) {
        var arity = args.length
        var ast = findFixedArity(arity, bodies) || findVariadic(bodies)
        return ast[1]
    }
    fn.gen_env = function (args) {
        var arity = args.length
        var body = findFixedArity(arity, bodies) || findVariadic(bodies)
        return new_env(env, body[0], args)
    }
    fn.ismacro = false;
    return fn;
}

// Keywords
export const _keyword = obj => _keyword_Q(obj) ? obj : '\u029e' + obj
export const _keyword_Q = obj => typeof obj === 'string' && obj[0] === '\u029e'

// Lists
export const _list_Q = obj => Array.isArray(obj) && !(obj instanceof Vector)

// Vectors
export class Vector extends Array { }

// Maps
export function _assoc_BANG(hm, ...args) {
    if (args.length % 2 === 1) {
        throw new Error('Odd number of assoc arguments')
    }
    let hamt = hm
    for (let i=0; i<args.length; i+=2) {
       hamt = hamt.set(args[i], args[i+1], hm)
    }
    return hamt
}

export function _hash_map_Q(hm) {
    return !!(hm && hm.__hamt_isMap)
}

// Sets
export function _set() {
    return new Set(arguments)
}

export function _set_Q(set) {
    return typeof set === "object" &&
        (set instanceof Set)
}

// Atoms
export class Atom {
    constructor(val) { this.val = val }
}

