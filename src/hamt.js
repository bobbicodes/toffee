const SIZE = 5;
const BUCKET_SIZE = Math.pow(2, SIZE);
const MASK = BUCKET_SIZE - 1;
const MAX_INDEX_NODE = BUCKET_SIZE / 2;
const MIN_ARRAY_NODE = BUCKET_SIZE / 4;

const hash = str => {
    const type = typeof str;
    if (type === 'number')
        return str;
    if (type === 'symbol')
        str = 'symbol:' + str.toString()
    if (type !== 'string')
        str += '';

    let hash = 0;
    for (let i = 0, len = str.length; i < len; ++i) {
        const c = str.charCodeAt(i);
        hash = (((hash << 5) - hash) + c) | 0;
    }
    //console.log('hash: ', hash)
    return hash;
};

const popcount = (x) => {
    x -= ((x >> 1) & 0x55555555);
    x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
    x = (x + (x >> 4)) & 0x0f0f0f0f;
    x += (x >> 8);
    x += (x >> 16);
    return (x & 0x7f);
};

const hashFragment = (shift, h) =>
    (h >>> shift) & MASK;

const toBitmap = x =>
    1 << x;

const fromBitmap = (bitmap, bit) =>
    popcount(bitmap & (bit - 1));

const arrayUpdate = (at, v, arr) => {
    const len = arr.length;
    const out = new Array(len);
    for (let i = 0; i < len; ++i)
        out[i] = arr[i];
    out[at] = v;
    return out;
};

const arraySpliceOut = (at, arr) => {
    const len = arr.length;
    const out = new Array(len - 1);
    let i = 0, g = 0;
    while (i < at)
        out[g++] = arr[i++];
    ++i;
    while (i < len)
        out[g++] = arr[i++];
    return out;
};

const arraySpliceIn = (at, v, arr) => {
    const len = arr.length;
    const out = new Array(len + 1);
    let i = 0, g = 0;
    while (i < at)
        out[g++] = arr[i++];
    out[g++] = v;
    while (i < len)
        out[g++] = arr[i++];
    return out;
};


function Map(root, size) {
    this.root = root;
    this.size = size;
};

Map.prototype.__hamt_isMap = true;

Map.prototype.setTree = function(root, size) {
    return root === this.root ? this : new Map(root, size);
};

export const empty = new Map(undefined, 0);

const LEAF = 1;
const COLLISION = 2;
const INDEX = 3;
const ARRAY = 4;

const Leaf = (hash, key, value) => ({
    type: LEAF,
    hash,
    key,
    value,
    _modify: Leaf__modify
});

const Collision = (hash, children) => ({
    type: COLLISION,
    hash,
    children,
    _modify: Collision__modify
});

const IndexedNode = (mask, children) => ({
    type: INDEX,
    mask,
    children,
    _modify: IndexedNode__modify
});

const empty__modify = (_, op, h, k, size) => {
    if (op.__hamt_delete_op)
        return undefined;
    const newValue = op.__hamt_set_op ? op.value : op();
    ++size.value;
    return Leaf(h, k, newValue);
};

const isLeaf = node =>
    (node.type === LEAF || node.type === COLLISION);

const Leaf__modify = function(shift, op, h, k, size) {
    if (k === this.key) {
        if (op.__hamt_delete_op) {
            --size.value;
            return undefined;
        }
        const currentValue = this.value;
        const newValue = op.__hamt_set_op ? op.value : op(currentValue);
        return newValue === currentValue ? this : Leaf(h, k, newValue);
    }
    if (op.__hamt_delete_op)
        return this;
    const newValue = op.__hamt_set_op ? op.value : op();
    ++size.value;
    return mergeLeaves(shift, this.hash, this, h, Leaf(h, k, newValue));
};

const Collision__modify = function(shift, op, h, k, size) {
    if (h === this.hash) {
        const list = updateCollisionList(this.hash, this.children, op, k, size);
        if (list === this.children)
            return this;

        return list.length > 1
            ? Collision(this.hash, list)
            : list[0]; // collapse single element collision list
    }
    if (op.__hamt_delete_op)
        return this;
    const newValue = op.__hamt_set_op ? op.value : op();
    ++size.value;
    return mergeLeaves(shift, this.hash, this, h, Leaf(h, k, newValue));
};

const IndexedNode__modify = function(shift, op, h, k, size) {
    const mask = this.mask;
    const children = this.children;
    const frag = hashFragment(shift, h);
    const bit = toBitmap(frag);
    const indx = fromBitmap(mask, bit);
    const exists = mask & bit;
    if (!exists) { // add
        const newChild = empty__modify(shift + SIZE, op, h, k, size);
        if (!newChild)
            return this;
        
        return children.length >= MAX_INDEX_NODE
            ? expand(frag, newChild, mask, children)
            : IndexedNode(
                mask | bit,
                arraySpliceIn(indx, newChild, children));
    }

    const current = children[indx];
    const newChild = current._modify(shift + SIZE, op, h, k, size);
    if (current === newChild)
        return this;

    if (!newChild) { // remove
        const bitmap = mask & ~bit;
        if (!bitmap) 
            return undefined;
        
        return children.length === 2 && isLeaf(children[indx ^ 1])
            ? children[indx ^ 1] // collapse
            : IndexedNode(
                bitmap,
                arraySpliceOut(indx, children));
    }

    // modify
    return op.__hamt_delete_op && children.length === 1 && isLeaf(newChild)
        ? newChild // propagate collapse
        : IndexedNode(
            mask,
            arrayUpdate(indx, newChild, children));
};

const mergeLeaves = (shift, h1, n1, h2, n2) => {
    if (h1 === h2)
        return Collision(h1, [n2, n1]);

    const subH1 = hashFragment(shift, h1);
    const subH2 = hashFragment(shift, h2);
    return IndexedNode(toBitmap(subH1) | toBitmap(subH2),
        subH1 === subH2
            ? [mergeLeaves(shift + SIZE, h1, n1, h2, n2)]
            : subH1 < subH2 ? [n1, n2] : [n2, n1]);
};

const updateCollisionList = (h, list, op, k, size) => {
    const len = list.length;
    for (let i = 0; i < len; ++i) {
        const child = list[i];
        if (child.key === k) {
            const value = child.value;
            if (op.__hamt_delete_op) {
                --size.value
                return arraySpliceOut(i, list);
            }
            const newValue = op.__hamt_set_op ? op.value : op(value);
            if (newValue === value)
                return list;
            return arrayUpdate(i, Leaf(h, k, newValue), list);
        }
    }

    if (op.__hamt_delete_op)
        return list;
    const newValue = op.__hamt_set_op ? op.value : op();
    ++size.value;
    return arrayUpdate(len, Leaf(h, k, newValue), list);
};

const modifyHash = (f, hash, key, map) => {
    const size = { value: map.size };
    const newRoot = map.root ? map.root._modify(0, f, hash, key, size) : empty__modify(0, f, hash, key, size);
    return map.setTree(newRoot, size.value);
};

Map.prototype.modifyHash = function(hash, key, f) {
    return modifyHash(f, hash, key, this);
};

const setHash = function(hash, key, value, map) {
    return modifyHash({ __hamt_set_op: true, value: value }, hash, key, map);
}

Map.prototype.setHash = function(hash, key, value) {
    return setHash(hash, key, value, this);
};

export const set = (key, value, map) =>
    setHash(hash(key), key, value, map);

Map.prototype.set = function(key, value) {
    return set(key, value, this);
};

const tryGetHash = (alt, hash, key, map) => {
    if (!map.root)
        return alt;

    let node = map.root;
    let shift = 0;
    while (true) switch (node.type) {
        case LEAF:
            {
                return key === node.key ? node.value : alt;
            }
        case COLLISION:
            {
                if (hash === node.hash) {
                    const children = node.children;
                    for (let i = 0, len = children.length; i < len; ++i) {
                        const child = children[i];
                        if (key === child.key)
                            return child.value;
                    }
                }
                return alt;
            }
        case INDEX:
            {
                const frag = hashFragment(shift, hash);
                const bit = toBitmap(frag);
                if (node.mask & bit) {
                    node = node.children[fromBitmap(node.mask, bit)];
                    shift += SIZE;
                    break;
                }
                return alt;
            }
        case ARRAY:
            {
                node = node.children[hashFragment(shift, hash)];
                if (node) {
                    shift += SIZE;
                    break;
                }
                return alt;
            }
        default:
            return alt;
    }
};

Map.prototype.tryGetHash = function(alt, hash, key) {
    return tryGetHash(alt, hash, key, this);
};

const tryGet = (alt, key, map) =>
    tryGetHash(alt, hash(key), key, map);

Map.prototype.tryGet = function(alt, key) {
    return tryGet(alt, key, this);
};

const get = (key, map) =>
    tryGetHash(undefined, hash(key), key, map);

Map.prototype.get = function(key, alt) {
    return tryGet(alt, key, this);
};

const appk = k =>
    k && lazyVisitChildren(k.len, k.children, k.i, k.f, k.k);

/**
    Recursively visit all values stored in an array of nodes lazily.
*/
const lazyVisitChildren = (len, children, i, f, k) => {
    while (i < len) {
        const child = children[i++];
        if (child)
            return lazyVisit(child, f, {len, children, i, f, k});
    }
    return appk(k);
};

const lazyVisit = (node, f, k) => {
    if (node.type === LEAF) 
        return { value: f(node), rest: k };

    const children = node.children;
    return lazyVisitChildren(children.length, children, 0, f, k);
};

const DONE = { done: true };

function MapIterator(v) {
    this.v = v;
};

MapIterator.prototype.next = function() {
    if (!this.v)
        return DONE;
    const v0 = this.v;
    this.v = appk(v0.rest);
    return v0;
};

MapIterator.prototype[Symbol.iterator] = function() {
    return this;
};

const visit = (map, f) =>
    new MapIterator(map.root ? lazyVisit(map.root, f) : undefined);

const buildPairs = (x) => [x.key, x.value];
const entries = (map) =>
    visit(map, buildPairs);

Map.prototype.entries = Map.prototype[Symbol.iterator] = function() {
    return entries(this);
};

const buildKeys = (x) => x.key;
const keys = (map) =>
    visit(map, buildKeys);

Map.prototype.keys = function() { return keys(this); }

const buildValues = x => x.value;
const values = Map.prototype.values = map =>
    visit(map, buildValues);

Map.prototype.values = function() {
    return values(this);
};

const nothing = ({});

const hasHash = (hash, key, map) =>
    tryGetHash(nothing, hash, key, map) !== nothing;

Map.prototype.hasHash = function(hash, key) {
    return hasHash(hash, key, this);
};

const has = (key, map) =>
    hasHash(hash(key), key, map);

Map.prototype.has = function(key) {
    return has(key, this);
};

const del = { __hamt_delete_op: true };
const removeHash = (hash, key, map) =>
    modifyHash(del, hash, key, map);

Map.prototype.removeHash = Map.prototype.deleteHash = function(hash, key) {
    return removeHash(hash, key, this);
};

const remove = (key, map) =>
    removeHash(hash(key), key, map);

Map.prototype.remove = Map.prototype.delete = function(key) {
    return remove(key, this);
};

const h = empty
const h2 = h.set('key', 'value')
const h3 = h2.set('key2', 'value2')

//console.log(h3.root)
//console.log(h2.has('key'))
//console.log(h3.size)
