import test from 'node:test';
import { strict as assert } from 'node:assert';
import {evalString} from "../index.js"

test('define and call function', (t) => {
  evalString('(defn add1 [n] (+ n 1))')
  const result = evalString('(add1 5)')
  assert.strictEqual(result, '6');
});

test('reads integers', async (t) => {
  await t.test('decimal', (t) => {
    const result = evalString('(read-string "42")')
    assert.strictEqual(result, '42');
  });
  await t.test('decimal positive', (t) => {
    const result = evalString('(read-string "+42")')
    assert.strictEqual(result, '42');
  });
  await t.test('decimal negative', (t) => {
    const result = evalString('(read-string "-42")')
    assert.strictEqual(result, '-42');
  });
  await t.test('hex', (t) => {
    const result = evalString('(read-string "0x42e")')
    assert.strictEqual(result, '1070');
  });
  await t.test('hex positive', (t) => {
    const result = evalString('(read-string "+0x42e")')
    assert.strictEqual(result, '1070');
  });
  await t.test('hex negative', (t) => {
    const result = evalString('(read-string "-0x42e")')
    assert.strictEqual(result, '-1070');
  });
}); 
