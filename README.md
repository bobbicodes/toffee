# Toffee Lisp [![NPM version](https://img.shields.io/npm/v/toffee-lisp?color=purple)](https://www.npmjs.com/package/toffee-lisp)

## Features

- Persistent hash array mapped tries
- Multi-arity functions
- loop/recur
- Destructuring

## Install

Requires [Node.js](https://nodejs.org/).

```
npm i toffee-lisp
```

## Usage

### REPL

```
node repl.js
```

### Run a script

```clojure
;; script.lisp

(defn yo [name]
  (str "what up " name))

(println (yo "dawg"))
```

```
$ node toffee.js script.lisp
what up dawg
```

### Use in ES6 module

```javascript
import {evalString} from "toffee-lisp"

// define a function
evalString('(defn add1 [n] (+ n 1))')

console.log(evalString('(add1 5)'))
// => 6
```

## Test

```
node test/test.js
```
