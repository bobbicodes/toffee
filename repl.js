import * as readline from 'node:readline';
import {REP} from './src/evaluator.js'

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'OHAI> ',
});

if (process.argv.length > 2) {
    env_set(repl_env, Symbol.for('*ARGV*'), process.argv.slice(3))
    REP(`(load-file "${process.argv[2]}")`)
    process.exit(0)
}

REP('(println (str "Toffee Lisp [" *host-language* "]"))')

rl.prompt();
rl.on('line', (line) => {
    try {
        if (line) { console.log(REP(line)) }
    } catch (exc) {
        if (exc instanceof Error) { console.warn(exc.stack) }
        else { console.warn(`Error: ${pr_str(exc, true)}`) }
    }
  rl.prompt();
}).on('close', () => {
  console.log('Have a great day!');
  process.exit(0);
});
