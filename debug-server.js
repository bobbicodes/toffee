import * as http from "http"
import * as hamt from "./src/hamt.js"
import {evalString} from "./index.js"

const host = 'localhost';
const port = 8000;

const hm = hamt.empty
const stringkey = "ehtdeth"
const symbolkey = Symbol.for('mysym')
const symbolmap = hamt.set(symbolkey, "value", hm)

let codeStr = `(do
(def client {:name "Super Co."
             :location "Philadelphia"
             :description "The worldwide leader in plastic tableware."})

(defn print-client [{name :name
         location :location
         description :description}]
  (println name location "-" description))
)`

evalString(codeStr)

const requestListener = function (req, res) {
  let message = evalString('(print-client client)')
  res.setHeader("Content-Type", "application/json");
  res.writeHead(200);
  res.end(`{"message": "${message}"}`);
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});
